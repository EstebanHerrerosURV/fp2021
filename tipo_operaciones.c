#include <stdio.h>

int sumar(int x, int y)
{
    int resultado;
    resultado = x + y;
    return resultado;
}
int restar(int x, int y)
{
    int resultado;
    resultado = x - y;
    return resultado;
}
int multiplicar(int x, int y)
{
    int resultado;
    resultado = x * y;
    return resultado;
}
int dividir(int x, int y)
{
    int resultado;
    //Cociente de la división
    resultado = x / y;
    return resultado;
}
int resto(int x, int y)
{
    int resultado;
    //Resto de la división
    resultado = x % y;
    return resultado;
}
int incrementar(int x)
{
    int resultado;
    x++;
    resultado = x;
    return resultado;
}
int decrementar(int x)
{
    int resultado;
    x--;
    resultado = x;
    return resultado;
}


int main()
{
    int x, y;
    printf("Bienvenido al programa de ejemplo de los tipos de operaciones.\n");
    printf("\n");
    printf("En primer lugar definirá el valor de dos variables(x, y).\n");
    printf("Seleccione un valor para 'x': \n");
    scanf("%d", &x);
    printf("Ahora seleccione un valor para 'y': \n");
    scanf("%d", &y);
    printf("A continuación verá el resultado de varias operaciones con dichas variables. \n");
    printf("\tSuma: %d\n", sumar(x, y));
    printf("\tResta: %d\n", restar(x, y));
    printf("\tMultiplicación: %d\n", multiplicar(x, y));
    printf("\tDivisión (cociente): %d\n", dividir(x, y));
    printf("\tDivisión (resto): %d\n", resto(x, y));
    printf("\tIncremento de x: %d\n", incrementar(x));
    printf("\tDecremento de x: %d\n", decrementar(x));

    return 0;
}
