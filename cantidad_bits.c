#include <stdio.h>
#include <math.h>

//Cálculo del logaritmo en base variable
double logaritmo_base(int numero, int base){
    double resultado;
    resultado = log(numero) / log(base);
    return resultado;
}

int main()
{
    int cifra_en_base_10;
    int bits;
    //Como el cálculo es para bits, la base es 2
    int base = 2;
    
    printf("Bienvenido a la calculadora de los bits necesarios para contar hasta una cifra.\n");
    printf("En primer lugar, escriba la cifra en base 10:\n");    
    scanf("%d", &cifra_en_base_10);
    
    double logaritmo = logaritmo_base(cifra_en_base_10, base);
    //Como el logaritmo puede tener decimales, estos se anulan haciendo un cast de double a int
    bits = (int)logaritmo + 1;
    
    printf("La cantidad de bits necesarios para contar hasta %d es %d", cifra_en_base_10, bits);

    return 0;
}
